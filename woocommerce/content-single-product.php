<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

	<?php
		/**
		 * Hook: woocommerce_before_single_product_summary.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary-wrapper">
		<div class="summary entry-summary outer-wrapper-3D">
			<div class="inner-wrapper-3D summary-inner-wrapper">
				<h1><?php the_title(); ?></h1>
				<?php if( get_field('subtitle') ): ?>
					<h2><?php the_field('subtitle'); ?></h2>
				<?php endif; ?>
				<?php the_excerpt(); ?>


				<?php
					/**
					 * Hook: woocommerce_single_product_summary.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */
					do_action( 'woocommerce_single_product_summary' );
				?>
				<p class="guarantee">enjoy a 30-day happiness guarantee</p>
			</div>
		</div>
	</div>

</div>

<div class="section-description">
	<div class="content">
		<div class="row">
			<div class="col-6">
				<h3>Description</h3>
			</div>
			<div class="col-3">
				<?php the_field('product_long_description'); ?>
			</div>
			<div class="col-3 information-outer-wrapper">
				<?php if( have_rows('other_information') ): ?>
					<?php while ( have_rows('other_information') ) : the_row(); ?>
						<div class="information-wrapper">
							<h4><?php the_sub_field('heading'); ?></h4>
							<?php if ( get_sub_field('content')) : ?>
								<p><?php the_sub_field('content'); ?></p>
							<?php endif; ?>
						</div>
				  <?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<div class="section-facts light">
	<?php get_template_part( 'template-parts/facts-bar' ); ?>
</div>

<div class="section-praise">
	<div class="content">
		<div class="row">
			<div class="col-6 video-wrapper">
				<h2>Praise</h2>

				<?php if( get_field('testimonial_video') ): ?>
					<video controls>
					  <source src="<?php the_field('testimonial_video'); ?>" type="video/mp4">
							Your browser does not support the video tag.
					</video>
				<?php endif; ?>


				<div class="review-button" id="open-review">Leave a Review</div>

			</div>
		</div>
	</div>
</div>

<?php
  $params = array('posts_per_page' => -1, 'post_type' => 'product', 'orderby' => 'menu_order', 'order' => 'ASC');
  $products_query = new WP_Query($params);
  if ($products_query->have_posts()) :
?>
	<div class="section-products">
		<div class="products-carousel will-slick">
			<?php while ($products_query->have_posts()) :
				$products_query->the_post();
				$product = get_product(get_the_ID());
			?>
			<div class="product">
				<a href="<?php the_permalink(); ?>">
					<div class="circle">
						<div class="zoom-tilt">
						<?php the_post_thumbnail(); ?>
					</div>
					</div>
				</a>
				<div class="product-info">
					<h3>
							<a href="<?php the_permalink(); ?>">
								<?php the_title(); ?>
								<?php if( get_field('subtitle') ): ?>
									<?php the_field('subtitle'); ?>
								<?php endif; ?>
							 </a>
					</h3>
					<h5>$<?php echo $product->get_price(); ?></h5>
				</div>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
<?php endif; wp_reset_postdata(); ?>

<?php if( have_rows('comparison_chart', 'option') ): ?>
  <div class="section-comparison">

		<div class="content">

		  <div class="row table-header">
				<div class="col-2 left">traditional cleansing</div>
		    <div class="col-2 right">just croon and water</div>
		  </div>

		  <div class="table-body">
				<?php while ( have_rows('comparison_chart', 'option') ) : the_row(); ?>

 	       <div class="row">
 	         <div class="col-2 label">
 	           <h4><?php the_sub_field('label'); ?></h4>
 	         </div>

 	         <div class="col-2 cell left">
 	           <?php if( have_rows('traditional_wipes') ): ?>
 	             <?php while ( have_rows('traditional_wipes') ) : the_row(); ?>
 	               <h5>
 	                 <?php the_sub_field('number'); ?>
 	                 <?php $options = get_sub_field('options'); ?>
 	                 <?php if ($options) { ?> <div class="options-wrapper"> <?php } ?>
 	                 <?php if ($options && in_array('approximate', $options)) { ?>
 	                   <div class="app">&#126;</div>
 	                 <?php } if ($options && in_array('dollars', $options)) { ?>
 	                   <div class="dollar-sign">&#36;</div>
 	                 <?php } if ($options) { ?> </div>
 	                 <?php } else { ?>
 	                 <div class="unit"><?php the_sub_field('unit'); ?></div>
 	               <?php } ?>
 	               </h5>
 	             <?php endwhile; ?>
 	           <?php endif; ?>
 	         </div>

 	         <div class="col-2 cell right">
 	           <?php if( have_rows('croon') ): ?>
 	             <?php while ( have_rows('croon') ) : the_row(); ?>
 	               <h5>
 	                 <?php the_sub_field('number'); ?>
 	                 <?php $options = get_sub_field('options'); ?>
 	                 <?php if ($options) { ?> <div class="options-wrapper"> <?php } ?>
 	                 <?php if ($options && in_array('approximate', $options)) { ?>
 	                   <div class="app">&#126;</div>
 	                 <?php } if ($options && in_array('dollars', $options)) { ?>
 	                   <div class="dollar-sign">&#36;</div>
 	                 <?php } if ($options) { ?> </div>
 	                 <?php } else { ?>
 	                 <div class="unit"><?php the_sub_field('unit'); ?></div>
 	               <?php } ?>
 	               </h5>
 	             <?php endwhile; ?>
 	           <?php endif; ?>
 	         </div>
 	       </div>

 	     <?php endwhile; ?>
		  </div>

		</div>

	</div>
<?php endif; ?>

<?php do_action( 'woocommerce_after_single_product' ); ?>
