<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

      $user_info = get_userdata(get_current_user_id());
      $first_name = $user_info->first_name;
			$last_name = $user_info->last_name;
			$email = $user_info->user_email;

?>

<?php
  /**
   * My Account content.
   *
   * @since 2.6.0
   */
  	do_action( 'woocommerce_account_dashboard' );
?>
