<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package croon
 */

get_header();
?>



	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="page-header">
				<h1>Lifestyle</h1>
			</div>

		<?php $the_query = new WP_Query( array('posts_per_page' => 0, 'post__in'  => get_option('sticky_posts'), 'orderby' => 'date',
    'order' => 'DESC',
    'posts_per_page' => 1,
    'ignore_sticky_posts' => 1 ) ); ?>

		<?php if ( $the_query->have_posts() ) : ?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post();
				$thumbnail_url = get_the_post_thumbnail_url(get_the_ID(),'full');
				$readtime = get_field( "reading_time" );
				$teaser = get_field( "post_teaser" );
				$featuredID = $post->ID;
			?>

				<div class="featured-post">

					<div class="image-wrapper">
						<a href="<?php the_permalink(); ?>">
							<div class="image" style="background-image: url('<?php echo $thumbnail_url; ?>')"></div>
						</a>
					</div>

						<div class="blog-post-outer-wrapper content">
							<div class="blog-post-wrapper row">
								<div class="circle-date">
										<span class="day">
											<?php echo get_the_date('j'); ?>
											<span class="month"><?php echo get_the_date('M'); ?></span>
										</span>
								</div>
								<div class="post-meta col-4">
									<?php if( $readtime ) { ?>
										<span class="read-time"><?php echo $readtime; ?> min read</span>
									<?php }  ?>
									<a href="<?php the_permalink(); ?>">
										<h4><?php the_title(); ?></h4>
									</a>
									<?php if( $teaser ) { ?>
										<div class="post-teaser"><?php echo $teaser; ?></div>
									<?php }  ?>
								</div>
							</div>
						</div>

					</div>

			<?php endwhile; ?>
		<?php endif; ?>

	<?php
		$main_query = new WP_Query( array('post__not_in' => array($featuredID),) );
		if ( $main_query->have_posts() ) :
	?>

			<div class="content">
				<div class="category-filter">
					<div class="category-inner-wrapper">
						<?php
						$categories = get_categories(array(
							'exclude' => 1,
						));
						foreach($categories as $category) {
						   echo '<div class="category-label" id="' . $category->slug . '">' . $category->name . '</div>';
						}
						?>
					</div>
					<div class="category-label show-all active">all</div>
				</div>
			</div>

			<div class="post-list">
				<div class="content">

					<?php while ( $main_query->have_posts() ) : $main_query->the_post();
						$thumbnail_url = get_the_post_thumbnail_url(get_the_ID(),'medium');
						$readtime = get_field( "reading_time" );
						$teaser = get_field( "post_teaser" );
					?>

							<div class="not-featured blog-post-wrapper row <?php foreach((get_the_category()) as $category){ echo "category-".$category->slug." "; }	?>">
								<div class="col-3">
									<div class="image-wrapper">
										<span class="month"><?php echo get_the_date('M'); ?></span>
										<a href="<?php the_permalink(); ?>">
											<div class="image" style="background-image: url('<?php echo $thumbnail_url; ?>')">
												<div class="circle-button right"></div>
												<span class="day"><?php echo get_the_date('j'); ?></span>
											</div>
										</a>
									</div>
								</div>
								<div class="post-meta col-3">
									<?php if( $readtime ) { ?>
										<span class="read-time"><?php echo $readtime; ?> min read</span>
									<?php }  ?>
									<a href="<?php the_permalink(); ?>">
								  	<h4><?php the_title(); ?></h4>
									</a>
									<?php if( $teaser ) { ?>
										<div class="post-teaser"><?php echo $teaser; ?></div>
									<?php }  ?>
								</div>
							</div>

					<?php endwhile; ?>

				</div>
			</div>

<?php endif; ?>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
