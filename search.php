<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package croon
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			<div class="content">
		    <div class="row tagline">
		      <div class="custom-tagline">
		        	<h1 class="outline-white" data-text="<?php echo get_search_query(); ?>">
							All we've got on <?php echo get_search_query(); ?>
						</h1>
		      </div>
		    </div>
		  </div>

			<div class="content news-preview">
			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();
			?>

			<a href="<?php the_permalink(); ?>" class="blog-post-wrapper">
				<div class="image-wrapper">
					<div class="image ratio-3-2" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')" alt="<?php the_title(); ?>"></div>
				</div>
				<div class="post-meta">
					<span class="date"><?php echo get_the_date(); ?></span>
					<h6><?php the_title(); ?></h6>
				</div>
			</a>

		<?php endwhile; ?>
		</div>

		<div class="content">
			<?php the_posts_navigation(); ?>
		</div>



			<div class="content search-bar-outer-wrapper">
				<?php get_search_form(); ?>
			</div>

		<?php else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
