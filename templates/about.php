<?php
/**
  * Template Name: About
 * @package croon
 */
get_header();
?>


<div class="page-header">
	<h1>About</h1>
</div>

<section id="the-content">

		<div class="content">
	    <div class="row col-6 above-product">
				<h2><?php the_field('above_product_heading'); ?></h2>
				<h3><?php the_field('above_product_subheading'); ?></h3>
			</div>
		</div>

		<?php $image = get_field('product_image');
	  $size = 'large'; // (thumbnail, medium, large, full or custom size)
	  if( $image ) { $url = wp_get_attachment_url( $image, $size ); }
	  $offset = get_field('offset', $image);
	  ?>



		<div class="section-product">
			<div class="horizontal-line"></div>
			<img src="<?php echo $url; ?>" class="the-product" style="transform: translateX(<?php echo $offset; ?>%)" />

			<div class="content">
				<div class="product-image">
			    <div class="row">
						<div class="col-6">
							<h2><?php the_field('product_heading'); ?></h2>
							<h3><?php the_field('product_subheading'); ?></h3>
						</div>
					</div>
			</div>
		</div>
	</div>


		<div class="marquee text-white font-ag ag9 ">
		  <div class="marquee-inner" aria-hidden="true">
		    <div class="text-wrapper"><?php the_field('marquee_text'); ?></div>
		    <div class="text-wrapper"><?php the_field('marquee_text'); ?></div>
		    <div class="text-wrapper"><?php the_field('marquee_text'); ?></div>
		    <div class="text-wrapper"><?php the_field('marquee_text'); ?></div>
		  </div>
		</div>

		<div class="content">
	    <div class="row">
				<div class="col-6 below-product">
					<h2><?php the_field('below_product_heading'); ?></h2>
					<h3><?php the_field('below_product_subheading'); ?></h3>
				</div>
			</div>
		</div>

		<div class="two-columns">
			<?php if( have_rows('left_column') ): ?>
				<div class="left column">
    			<?php while( have_rows('left_column') ): the_row();
					$image = get_sub_field('image'); $size = 'medium';
				  if( $image ) { $image_url = wp_get_attachment_url( $image, $size ); }
					?>
						<h3><?php the_sub_field('heading'); ?></h3>
						<div class="featured-image" style="background-image: url('<?php echo $image_url; ?>');"></div>
						<div class="accreditations">
							<?php while ( have_rows( 'accredications' ) ) : the_row();
							$icon = get_sub_field('icon'); $size = 'small';
						  if( $icon ) { $icon_url = wp_get_attachment_url( $icon, $size ); }
							?>
								<div class="accreditation">
									<div class="icon"><img src="<?php echo $icon_url; ?>" /></div>
									<div class="accreditation-content">
										<h4><?php the_sub_field( 'heading' ); ?></h4>
										<p><?php the_sub_field( 'text' ); ?></h4>
									</div>
								</div>
	     				<?php endwhile; ?>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>

			<?php if( have_rows('right_column') ): ?>
				<div class="right column">
    			<?php while( have_rows('right_column') ): the_row();
					$image = get_sub_field('image'); $size = 'medium';
				  if( $image ) { $image_url = wp_get_attachment_url( $image, $size ); }
					?>
						<h3><?php the_sub_field('heading'); ?></h3>
						<div class="featured-image" style="background-image: url('<?php echo $image_url; ?>');"></div>
						<div class="accreditations">
							<?php while ( have_rows( 'accredications' ) ) : the_row();
							$icon = get_sub_field('icon'); $size = 'small';
						  if( $icon ) { $icon_url = wp_get_attachment_url( $icon, $size ); }
							?>
								<div class="accreditation">
									<div class="icon"><img src="<?php echo $icon_url; ?>" /></div>
									<div class="accreditation-content">
										<h4><?php the_sub_field( 'heading' ); ?></h4>
										<p><?php the_sub_field( 'text' ); ?></h4>
									</div>
								</div>
	     				<?php endwhile; ?>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>

		</div>

		<div class="section-teal-row">
			<div class="content">
				<div class="row">
					<div class="col-4 teal-inner-wrapper">
						<h3><?php the_field('teal_heading'); ?></h3>
						<p><?php the_field('teal_text'); ?></p>
					</div>
				</div>
			</div>
		</div>

		<div class="section-upcycle">
			<div class="content">
				<h3><?php the_field('upcycle_heading'); ?></h3>
				<div class="steps steps-carousel will-slick">
					<div class="step"><div class="step-content"><?php the_field('step_1'); ?></div></div>
					<div class="step"><div class="step-content"><?php the_field('step_2'); ?></div></div>
					<div class="step"><div class="step-content"><?php the_field('step_3'); ?></div></div>
				</div>
			</div>
		</div>


</section>


<?php get_footer();
