<?php
/**
  * Template Name: Account
 * @package croon
 */
get_header();
$image = get_field('login_background_image', 'option');
$size = 'large';
if( $image ) { $url = wp_get_attachment_url( $image, $size ); }
?>

<div class="page-body" style="background-image: url(<?php echo $url; ?>)">
<div class="page-header">
	<h1>Account</h1>
</div>

<section id="the-content">
	<div class="content">
			<?php
				while ( have_posts() ) :
					the_post();
					the_content();
				endwhile;
			?>
			<!-- close col-4 dive -->
			<div class="row mobile-logout">
				<div class="col-6">
					<a href="<?php echo wp_logout_url(); ?>">Logout</a>
				</div>
			</div>

		</div>
	</div>
</section>
</div>

<!-- get_sidebar(); -->
<?php
get_footer();
