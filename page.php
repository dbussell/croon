<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package croon
 */

get_header();

?>



<section id="the-content">
	<div class="content">

			<div class="row">
				<div class="col-4 inner-wrapper">
					<h1><?php echo get_the_title(); ?></h1>

				<?php
				while ( have_posts() ) :
					the_post();

					the_content();

				?>
				<?php  endwhile; ?>
			</div>
			</div>
	</div>
</section>

<!-- get_sidebar(); -->
<?php
get_footer();
