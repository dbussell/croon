import $ from 'jquery';
import 'slick-carousel';

$(document).ready(function(){

  setTimeout(function(){ $('.will-slick:not(.slick-slider)').css('opacity','1'); },500)

  $('.cycle-carousel').slick({
    asNavFor: '.section-instructions',
    speed: 500,
    fade: true,
    mobileFirst: true,
    cssEase: 'linear',
    arrows: false,
    dots: true,
    responsive: [
      {
        breakpoint: 900,
        settings: {
          arrows: true,
          dots: false,
        }
      }
    ]
  });

  $('.section-instructions').slick({
    autoplay: false,
    arrows: false,
    dots: false,
    fade: true,
    focusOnSelect: true,
    asNavFor: '.cycle-carousel',
    slidesToShow: 4,
    variableWidth: true,
    centerMode: true,
  });

  $('.account-navigation-carousel').slick({
    autoplay: false,
    arrows: false,
    dots: false,
    variableWidth: true,
    infinite: false,
    mobileFirst: true,
    touchThreshold: 2,
    responsive: [
      {
        breakpoint: 480,
        settings: "unslick"
      }
    ]
  });

  $('.products-carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    mobileFirst: true,
    dots: true,
    arrows: false,
    fade: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          autoplay: false,
          arrows: true,
          fade: false,
          dots: false,
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      }
    ]
  });

  $('.steps-carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    mobileFirst: true,
    dots: true,
    arrows: false,
    fade: true,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          fade: false,
          slidesToShow: 3,
          dots: false
        }
      }
    ]
  });

  $('.products-bar-inner-wrapper').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    mobileFirst: true,
    arrows: true,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 720,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        }
      }
    ]
  });


  // Praise

  $('.praise-carousel-controls').slick({
    asNavFor: '.praise-carousel',
    slidesToShow: 4,
    autoplay: false,
    arrows: false,
    dots: false,
    fade: true,
    focusOnSelect: true,
    variableWidth: true,
  });

  $('.fact-wrapper').slick({
    mobileFirst: true,
    slidesToShow: 1,
    autoplay: true,
    arrows: false,
    dots: true,
    fade: true,
    responsive: [
      {
        breakpoint: 480,
        settings: "unslick"
      }
    ]
  });

  $('.circles-wrapper').slick({
    mobileFirst: true,
    slidesToShow: 1,
    autoplay: false,
    speed: 2000,
    arrows: false,
    dots: true,
    fade: true,
    initialSlide: 1,
    responsive: [
      {
        breakpoint: 768,
        settings: "unslick",
      }
    ]
  });


  $('.praise-carousel').slick({
    autoplay: false,
    arrows: false,
    dots: false,
    fade: true,
    asNavFor: '.praise-carousel-controls',
    slidesToShow: 1,
    centerMode: true,
  });

  $('.more-posts').slick({
    autoplay: false,
    mobileFirst: true,
    arrows: false,
    dots: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          arrows: false,
          dots: true,
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 960,
        settings: {
          arrows: true,
          dots: false,
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
    ]
  });

  $( "#review_form_wrapper" ).wrap( "<div class='review-form-outer-wrapper'></div>" );
  $( ".review-form-click-wrapper" ).insertBefore( "#review_form_wrapper" );


  $(".section-hero").interactive_bg({
   strength: 25,
   scale: 1.0,
   animationSpeed: "100ms",
   contain: true,
   wrapContent: false
 });


 $( ".email-popup-wrapper .tag-mobile" ).on('touchstart click', function(){
   $(".email-popup-wrapper").removeClass("open");
   Cookies.set('email-popup', 'close', { expires: 365 });
   if (event.type == "touchstart") {
     $(this).off('click');
   }
 });


});

$(window).scroll(function() {
    if($(window).scrollTop() > 200){
       $('.hero-text').addClass("slideInUp");
     }
    if($(window).scrollTop() > 200){
         $('body').addClass("scrolled");
         $('.header-bar').addClass("scrolled");
         $('.products-bar').removeClass("bounceInUp").addClass("slideOutDown");
    }
    else {
      $('body').removeClass("scrolled");
      $('.header-bar').removeClass("scrolled");
      $('.products-bar').addClass("bounceInUp").removeClass("slideOutDown");
      $('.hero-text').removeClass("slideInUp");
    }
});

$(function() {
    var $el = $('.parallax-background');
    $(window).on('scroll', function () {
        var scroll = $(document).scrollTop();
        $el.css({
            'background-position':'100% '+(50 - .075*scroll)+'%'
        });
    });
});


  $( ".cart-close-inner-wrapper" ).on('touchstart click', function(){
    $( ".cart-wrapper" ).removeClass("open");
    $( "body" ).removeClass('cart-open');
    if (event.type == "touchstart") {
      $(this).off('click');
    }
  });

  $( ".show-coupon" ).on('touchstart click', function(){
    $( ".email-popup-wrapper" ).toggleClass("open");
    if (event.type == "touchstart") {
      $(this).off('click');
    }
  });

  $( "#cart-input" ).on('touchstart click', function(){
    $( ".cart-wrapper" ).toggleClass('open');
    $( "body" ).toggleClass('cart-open');
    if (event.type == "touchstart") {
      $(this).off('click');
    }
  });

  $( "#menu-input" ).on('touchstart click', function(){
    $( ".cart-wrapper" ).removeClass('open');
    $( "body" ).removeClass('cart-open');
    if (event.type == "touchstart") {
      $(this).off('click');
    }
  });

  $( ".video-button" ).on('touchstart click', function(){
    $(".video-popup").addClass("open");
    if (event.type == "touchstart") {
      $(this).off('click');
    }
  });

  $( ".video-popup-click-wrapper" ).on('touchstart click', function(){
    $(".video-popup").removeClass("open");
    $(".video-popup").removeClass("play");
    $("#the-video").trigger('pause');
    if (event.type == "touchstart") {
      $(this).off('click');
    }
  });

  $( "#the-video" ).on('touchstart click', function(){
    $(".video-popup").toggleClass("play");
    $(".video-text-wrapper").removeClass("show");
    if (event.type == "touchstart") {
      $(this).off('click');
    }
  });

  $( ".email-popup-click-wrapper" ).on('touchstart click', function(){
    $(".email-popup-wrapper").removeClass("open");
    Cookies.set('email-popup', 'close', { expires: 365 });
    if (event.type == "touchstart") {
      $(this).off('click');
    }
  });

  $( ".email-popup-wrapper .close-wrapper" ).on('touchstart click', function(){
    $(".email-popup-wrapper").removeClass("open");
    Cookies.set('email-popup', 'close', { expires: 365 });
    if (event.type == "touchstart") {
      $(this).off('click');
    }
  });

  $('.category-label').on('touchstart click', function(){

    var clickID = $(this).attr("id");
    $('.category-label').removeClass("active");
    $(this).addClass("active");
    $('.not-featured').css("display", "none");
    $('.not-featured.category-' + clickID).css("display", "flex");

    if( $(this).hasClass("show-all") ) {
      $('.not-featured').css("display", "flex");
    }

    if (event.type == "touchstart") {
      $(this).off('click');
    }

  });





if ( !Cookies.get('email-popup') ) {
  setTimeout(function () {
    $( ".email-popup-wrapper" ).delay( 5000 ).addClass("open");
  }, 5000);

}



$('.video-popup, .email-popup-wrapper, .review-form-outer-wrapper').on('scroll touchmove mousewheel', function(e){
  e.preventDefault();
  e.stopPropagation();
  return false;
})

$('#the-video').hover(function toggleControls() {
    if (this.hasAttribute("controls")) {
        this.removeAttribute("controls")
    } else {
        this.setAttribute("controls", "controls")
    }
})

$('.review-button').on('touchstart click', function(){
  $('.contribution-form-wrapper').addClass("open");
  $('.review-form-outer-wrapper').addClass("open");
  if (event.type == "touchstart") {
    $(this).off('click');
  }
});

$('.review-form-outer-wrapper').on('touchstart click', function(){
  this.removeClass("open");
  if (event.type == "touchstart") {
    $(this).off('click');
  }
});

$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
});

var vids = $("video");
$.each(vids, function(){
       this.controls = false;
});
