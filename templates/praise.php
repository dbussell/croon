<?php
/**
  * Template Name: Praise
 * @package croon
 */
get_header();
?>

<div class="page-header">
	<h1>Praise</h1>
</div>

<section id="the-content">

	<div class="section-press">
		<div class="content">
	    <div class="row">
	      <h1>Praise for Croon</h1>

				<?php if( have_rows('praise') ): ?>

					<div class="praise-carousel col-6 will-slick">
						<?php while ( have_rows('praise') ) : the_row(); ?>
							<?php if( get_sub_field('link') ): ?>
								<a href="<?php the_sub_field('link'); ?>" target=_blank>
							<?php endif; ?>
							<p class="quote"><?php the_sub_field('short_quote'); ?></p>
							<?php if( get_sub_field('link') ): ?>
								</a>
							<?php endif; ?>
					  <?php endwhile; ?>
					</div>

					<div class="praise-carousel-controls col-6 will-slick">
						<?php while ( have_rows('praise') ) : the_row(); ?>
							<img class="logo" src="<?php the_sub_field('logo'); ?>" alt="<?php the_sub_field('publication_name'); ?>"/>
					  <?php endwhile; ?>
					</div>

				<?php endif; ?>

	    </div>
		</div>
	</div>


	<?php if( have_rows('other_praise') ): ?>

		<div class="section-reviews">
			<div class="content">


				<div class="row">
					<?php while ( have_rows('featured_praise') ) : the_row(); ?>
						<div class="featured-praise">
						<div class="col-6 comment-text">
							<div class="description"><?php the_sub_field("review_text"); ?></div>
							<div class="woocommerce-review__author">
								<div class="review-meta">
									<span class="name"><?php the_sub_field("reviewer_name"); ?></span>
									<?php if (get_field('reviewer_location')) : ?>
										<span class="location">,&nbsp;<?php the_sub_field("reviewer_location"); ?></span>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<?php endwhile; ?>
				</div>

			<div class="row">
				<?php while ( have_rows('other_praise') ) : the_row(); ?>
					<div class="col-3 comment-text">
						<div class="description"><?php the_sub_field("review_text"); ?></div>
						<div class="woocommerce-review__author">
							<div class="review-meta">
								<span class="name"><?php the_sub_field("reviewer_name"); ?></span>
								<span class="location">,&nbsp;<?php the_sub_field("reviewer_location"); ?></span>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>


			</div>
		</div>

	<?php endif; ?>

	<div class="facts-wrapper">
		<div class="section-facts dark">
		  <?php get_template_part( 'template-parts/facts-bar' ); ?>
		</div>
	</div>


<?php get_template_part( 'template-parts/video-feature' ); ?>




</section>


<?php get_footer();
