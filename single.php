<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package croon
 */

get_header();
$readtime = get_field( "reading_time" );
?>

<div class="page-header">
	<h1 class="header">Lifestyle</h1>
</div>

<div class="image-wrapper">
		<div class="image" style="background-image: url('<?php echo get_the_post_thumbnail_url( null, 'full' ); ?>')"></div>
</div>

<section id="the-content">
		<div class="content">

			<?php while ( have_posts() ) : the_post(); ?>

			<div class="row">
				<div class="col-6">
					<div class="circle-date">
							<span class="day">
								<?php echo get_the_date('j'); ?>
								<span class="month"><?php echo get_the_date('M'); ?></span>
							</span>
					</div>
					<?php if( $readtime ) { ?>
						<span class="read-time"><?php echo $readtime; ?> min</span>
					<?php } ?>
					<h1><?php the_title(); ?></h1>
					<div class="meta">
						<h3>by <?php the_author(); ?></h3>
						<div class="categories">
							<?php foreach((get_the_category()) as $category){
				        echo "<h5>".$category->name."</h5>";
			        }	?>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-4 post-body">
					<?php	the_content(); ?>
				</div>
			</div>

			<?php
				endwhile;
				wp_reset_postdata();
			?>

			<?php
				$args = array( 'post_type' => 'post' );
				$the_query = new WP_Query($args);
				if ( $the_query->have_posts() ) :
			?>
				<div class="row more-posts will-slick">
					<?php while ( $the_query->have_posts() ) : $the_query->the_post();
						$readtime = get_field( "reading_time" );
						$thumbnail_url = get_the_post_thumbnail_url(get_the_ID(),'small');
					?>
						<div class="col-3 post">
							<div class="thumbnail" style="background-image: url('<?php echo $thumbnail_url; ?>')"></div>
							<div class="post-info">
								<h5><?php the_title(); ?></h5>
								<h6><?php echo $readtime; ?> min</h6>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; wp_reset_postdata(); ?>

		</div>

		<?php
			$image = get_field('background_image_post_single', 'option');
			$size = 'large'; // (thumbnail, medium, large, full or custom size)
			if( $image ) { $url = wp_get_attachment_url( $image, $size ); }
		?>

		<?php if ( $image ) : ?>
		  <div class="video-feature" style="background-image: url('<?php echo $url; ?>');">
		    <h4 class="font-ag ag8 text-centered text-white">Do more with <span class="text-blue-dark">less.</span></h4>
				<div class="circle-button right video-button"></div>
			</div>
		<?php endif; ?>

		<?php get_template_part( 'template-parts/video-feature' ); ?>

</section>

<!-- get_sidebar(); -->
<?php
get_footer();
