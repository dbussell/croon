<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package croon
 */

?>

<div class="content">

  <div class="fact-wrapper will-slick">

    <div class="fact">
      <?php if( have_rows('fact_1', 'option') ): ?>
        <?php while( have_rows('fact_1', 'option') ): the_row(); ?>
            <img src="<?php echo get_sub_field('icon'); ?>" class="icon-light">
            <img src="<?php echo get_sub_field('icon_dark'); ?>" class="icon-dark">
          <?php echo get_sub_field('text'); ?>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>

    <div class="fact">
      <?php if( have_rows('fact_2', 'option') ): ?>
        <?php while( have_rows('fact_2', 'option') ): the_row(); ?>
          <img src="<?php echo get_sub_field('icon'); ?>" class="icon-light">
          <img src="<?php echo get_sub_field('icon_dark'); ?>" class="icon-dark">
          <?php echo get_sub_field('text'); ?>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>

    <div class="fact">
      <?php if( have_rows('fact_3', 'option') ): ?>
        <?php while( have_rows('fact_3', 'option') ): the_row(); ?>
          <img src="<?php echo get_sub_field('icon'); ?>" class="icon-light">
          <img src="<?php echo get_sub_field('icon_dark'); ?>" class="icon-dark">
          <?php echo get_sub_field('text'); ?>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>

  <a href="<?php the_field('facts_link', 'option'); ?>"><div class="circle-button right"></div></a>

</div>
