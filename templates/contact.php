<?php
/**
  * Template Name: Contact
 * @package croon
 */
get_header();
?>

<div class="page-header">
	<h1>Info</h1>
</div>

<div class="content">
	<div class="row col-6 sticker-wrapper">
		<?php if (get_field('sticker_text')) { ?>
			<div class="sticker"><?php the_field('sticker_text'); ?></div>
		<?php } ?>
	</div>
</div>


<section id="the-content">
	<div class="content">

    <div class="row">
      <h1><?php echo get_the_title(); ?></h1>
    </div>

			<div class="row">
        <div class="col-2 inner-wrapper sidebar">

          <?php if (get_field('address')) { ?>
            <div class="address">
              <h5>Our Address</h5>
              <p><?php the_field('address'); ?></p>
            </div>
          <?php } ?>

          <?php if (have_rows('upcycling_steps')):
            while( have_rows('upcycling_steps') ): the_row(); ?>
            <div class="upcycling-instructions">
              <h5>Upcycling</h5>
                <ol class="steps">
                  <li><?php the_sub_field('step_1'); ?></li>
                  <li><?php the_sub_field('step_2'); ?></li>
                  <li><?php the_sub_field('step_3'); ?></li>
                </ol>
              </div>
          <?php
            endwhile;
            endif;
          ?>

        </div>
				<div class="col-4 inner-wrapper contact-form">
				<?php
				while ( have_posts() ) :
					the_post();
					the_content();
				?>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>

<div class="section-facts neutral">
  <?php get_template_part( 'template-parts/facts-bar' );           // Navigation bar (nav.php) ?>
</div>

<!-- get_sidebar(); -->
<?php
get_footer();
