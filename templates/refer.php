<?php
/**
  * Template Name: Refer
 * @package croon
 */
get_header();
$image = get_field('login_background_image', 'option');
$size = 'large';
if( $image ) { $url = wp_get_attachment_url( $image, $size ); }
?>

<div class="page-body" style="background-image: url(<?php echo $url; ?>)">
<div class="page-header">
	<h1>Refer a Friend</h1>
</div>

<section id="the-content">
	<div class="content">

		<h2>Refer a friend to receive store credit!</h2>
			<?php
				while ( have_posts() ) :
					the_post();
					the_content();
				endwhile;
			?>


		</div>
	</div>
</section>
</div>

<!-- get_sidebar(); -->
<?php
get_footer();
