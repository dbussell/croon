<?php
/**
  * Template Name: How To
 * @package croon
 */
get_header();
?>

<div class="page-header">
	<h1>Info</h1>
</div>


<section id="the-content">

	<div class="content">
    <div class="row">
      <h1>How To Croon</h1>
    </div>
	</div>

	<?php if( have_rows('steps') ): ?>

		<div class="section-instructions will-slick">
			<?php while ( have_rows('steps') ) : the_row(); ?>
				<div class="step">
					<h2><?php the_sub_field('step_name'); ?></h2>
				</div>
		   <?php endwhile; ?>
		</div>

		<div class="section-cycle">
			<div class="semicircle cycle-carousel will-slick">
				<?php while ( have_rows('steps') ) : the_row();
					$image = get_sub_field('step_image');
					$offset = get_field('offset', $image);
				?>
					<div class="step">
						<img src="<?php echo wp_get_attachment_url( $image ); ?>" style="transform: translateX(<?php echo $offset; ?>%)" />
						<p><?php the_sub_field('step_text'); ?></p>
					</div>
				<?php endwhile; ?>
			</div>
		</div>

	<?php endif; ?>

	<?php if( have_rows('upcycle_instructions') ): ?>
		<div class="upcycle-instructions">
			<div class="content">

				<?php while ( have_rows('upcycle_instructions') ) : the_row(); ?>
						<h2><?php the_sub_field('section_title'); ?></h2>
						<p><?php the_sub_field('section_text'); ?></p>
			   <?php endwhile; ?>

			</div>
		</div>
	<?php endif; ?>


</section>


<?php get_footer();
