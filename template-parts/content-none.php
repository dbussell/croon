<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package croon
 */

?>

<section class="no-results not-found">
	<div class="content">
		<div class="row tagline">
			<div class="custom-tagline">
					<h1 class="outline-white" data-text="<?php echo get_search_query(); ?>">
					Nothing here about <br />
					<?php echo get_search_query(); ?>
				</h1>
			</div>
		</div>
	</div>

	<div class="content">
			<h5 class="white"><?php esc_html_e( 'Please try again with different keywords.', 'croon' ); ?></p>
	</div>

	<div class="content search-bar-outer-wrapper">
		<?php get_search_form(); ?>
	</div>

</section><!-- .no-results -->
