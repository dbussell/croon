<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package croon
 */

 $image = get_field('video_feature_background');
 $size = 'large'; // (thumbnail, medium, large, full or custom size)
 if( $image ) { $url = wp_get_attachment_url( $image, $size ); }

?>

<?php if ( $image ) : ?>
  <div class="video-feature" style="background-image: url('<?php echo $url; ?>');">
    <h4 class="font-ag ag8 text-centered text-white">Do more with <span class="text-teal">less.</span></h4>
    <div class="circle-button right video-button hvr-pop"></div>
  </div>
<?php endif; ?>

<?php if( get_field('video', 'option') ): ?>
<div class="video-popup">
  <?php get_template_part( 'template-parts/video-modal' ); ?>
</div>
<?php endif; ?>
