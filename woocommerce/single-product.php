<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>



<div class="page-header">
	<h1>Shop</h1>
</div>

	<div class="gradient-wrapper">
			<div class="circle"></div>
	</div>

<div class="email-popup-wrapper">
	<div class="email-popup-click-wrapper click-wrapper"></div>

	<div id="email-popup">
		<div class="tag">
			<?php get_template_part( 'assets/close-button' ); ?>
		</div>
		<div class="email-popup-circle">
		<div class="email-popup-inner-wrapper">

			<div class="tag-mobile">
				<?php get_template_part( 'assets/close-button' ); ?>
			</div>

		<h2>New to Croon?</h2>
		<p>Submit your email to keep in touch!</p>

		<div class="email-form">
				<!-- Begin Mailchimp Signup Form -->
				<div id="mc_embed_signup">
				<form action="https://justcroon.us19.list-manage.com/subscribe/post?u=9c9b212e9e0def0dc8d50aca6&amp;id=4959b8bdca" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
						<div id="mc_embed_signup_scroll">

				<div class="mc-field-group">
					<input type="email" value="" placeholder="email" name="EMAIL" class="required email input" id="mce-EMAIL">
				</div>
					<div id="mce-responses" class="clear">
						<div class="response" id="mce-error-response" style="display:none"></div>
						<div class="response" id="mce-success-response" style="display:none"></div>
					</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
						<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_b24cc371b47bd637c3f9159a0_780c25d796" tabindex="-1" value=""></div>
						<div class="clear button-wrapper"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
						</div>
				</form>
				</div>
				<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
				<!--End mc_embed_signup-->
			</div>


		</div>
	</div>
	</div>


	</div>



		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>


<?php
get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
