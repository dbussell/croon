<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package croon
 */

?>

<div class="content">

  <div class="row table-header">
    <div class="col-2 left">traditional cleansers</div>
    <div class="col-2 right">just croon and water</div>
  </div>

  <div class="table-body">
    <?php while ( have_rows('comparison_chart') ) : the_row(); ?>

      <div class="row">
        <div class="col-2 label">
          <h4><?php the_sub_field('label'); ?></h4>
        </div>

        <div class="col-2 cell left">
          <?php if( have_rows('traditional_wipes') ): ?>
            <?php while ( have_rows('traditional_wipes') ) : the_row(); ?>
              <h5>
                <?php the_sub_field('number'); ?>
                <?php $options = get_sub_field('options'); ?>
                <?php if ($options) { ?> <div class="options-wrapper"> <?php } ?>
                <?php if ($options && in_array('approximate', $options)) { ?>
                  <div class="app">&#126;</div>
                <?php } if ($options && in_array('dollars', $options)) { ?>
                  <div class="dollar-sign">&#36;</div>
                <?php } if ($options) { ?> </div>
                <?php } else { ?>
                <div class="unit"><?php the_sub_field('unit'); ?></div>
              <?php } ?>
              </h5>
            <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div class="col-2 cell right">
          <?php if( have_rows('croon') ): ?>
            <?php while ( have_rows('croon') ) : the_row(); ?>
              <h5>
                <?php the_sub_field('number'); ?>
                <?php $options = get_sub_field('options'); ?>
                <?php if ($options) { ?> <div class="options-wrapper"> <?php } ?>
                <?php if ($options && in_array('approximate', $options)) { ?>
                  <div class="app">&#126;</div>
                <?php } if ($options && in_array('dollars', $options)) { ?>
                  <div class="dollar-sign">&#36;</div>
                <?php } if ($options) { ?> </div>
                <?php } else { ?>
                <div class="unit"><?php the_sub_field('unit'); ?></div>
              <?php } ?>
              </h5>
            <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>

    <?php endwhile; ?>
  </div>

</div>
