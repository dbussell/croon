<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package croon
 */

get_header();
?>

<div class="page-header">
	<h1>404</h1>
</div>

<section class="error-404 not-found">
	<div class="circle big">
		<div class="circle small"></div>
	</div>
	<div class="content message">
		<h1>We can't find that.</h1>
		<h5>Why not return home?</h5>
	</div>
</section>

<?php
get_footer();
