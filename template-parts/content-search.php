<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package croon
 */

?>

<a href="<?php the_permalink(); ?>" class="blog-post-wrapper">
	<div class="post-meta">
		<span class="date"><?php echo get_the_date(); ?></span>
		<h6><?php the_title(); ?></h6>
	</div>
</a>
