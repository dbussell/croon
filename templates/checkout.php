<?php
/**
  * Template Name: Checkout
 * @package croon
 */


get_header();
$image = get_field('order_confirmation_background_image', 'option');
$size = 'large';
if( $image ) { $url = wp_get_attachment_url( $image, $size ); }

?>

<div class="page-body" style="background-image: url(<?php echo $url; ?>)">


<div class="page-header">
	<h1>Checkout</h1>
</div>

<?php
	while ( have_posts() ) :
		the_post();
		the_content();
	endwhile;
?>

</div>

<!-- get_sidebar(); -->
<?php
get_footer();
