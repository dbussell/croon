<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package croon
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">


		<div class="content navigation-wrapper">
			<nav class="footer-navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'footer_menu' ) ); ?>
			</nav>
		</div>

		<div class="bottom-row">

			<?php if ( have_rows('social_media_links', 'option') ) : ?>
				<div class="social-links">
				<?php while ( have_rows('social_media_links', 'option') ) : the_row(); ?>
					<a href="<?php the_sub_field('profile_url'); ?>" alt="<?php the_sub_field('platform'); ?>" target="_blank">
						<img src="<?php the_sub_field('icon'); ?>" alt="<?php the_sub_field('platform'); ?>" />
					</a>
				<?php endwhile; ?>
				</div>
			<?php endif; ?>

			<nav class="footer-secondary-navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'footer_secondary' ) ); ?>
			</nav>

			<div class="email-form">
					<!-- Begin Mailchimp Signup Form -->
					<div id="mc_embed_signup">
					<form action="https://justcroon.us19.list-manage.com/subscribe/post?u=9c9b212e9e0def0dc8d50aca6&amp;id=4959b8bdca" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					    <div id="mc_embed_signup_scroll">

					<div class="mc-field-group">
						<input type="email" value="" placeholder="enter your email to keep in touch" name="EMAIL" class="required email input" id="mce-EMAIL">
					</div>
						<div id="mce-responses" class="clear">
							<div class="response" id="mce-error-response" style="display:none"></div>
							<div class="response" id="mce-success-response" style="display:none"></div>
						</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_b24cc371b47bd637c3f9159a0_780c25d796" tabindex="-1" value=""></div>
					    <div class="clear button-wrapper"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button circle-button right"><div class="button-arrow"></div></div>
					    </div>
					</form>
					</div>
					<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
					<!--End mc_embed_signup-->

			</div>


		</div>

	</footer><!-- #colophon -->
</div><!-- #page -->


<?php wp_footer(); ?>

</body>
</html>
