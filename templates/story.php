<?php
/**
  * Template Name: Story
 * @package croon
 */
get_header();

$family = get_field('image');
$size = 'medium'; // (thumbnail, medium, large, full or custom size)
if( $family ) { $family_url = wp_get_attachment_url( $family, $size ); }

$signature = get_field('signature');
$size = 'small';
if( $signature ) { $signature_url = wp_get_attachment_url( $signature, $size ); }

?>

<div class="page-header">
	<h1>About</h1>
</div>

<section id="the-content">

	<div class="content">
			<div class="row">
				<div class="col-2">
					<?php if( $family ) { ?>
						<div class="family-image">
							<img src="<?php echo $family_url; ?>" />
							<?php if (get_field('sticker_text')) { ?>
		            <div class="sticker"><?php the_field('sticker_text'); ?></div>
		          <?php } ?>
						<?php } ?>
					</div>
				</div>
				<div class="col-4 story-wrapper">
					<div class="outer-wrapper-3D">
						<div class="inner-wrapper-3D">
							<h2>Our Story</h2>
								<?php while ( have_posts() ) :
									the_post();
									the_content();
								?>
								<h5>xx</h5>
				        <img class="signature" src="<?php echo $signature_url; ?>" />
							<?php endwhile; ?>
						</div>
					</div>
			</div>
		</div>
	</div>

	<div class="section-facts lightest">
	  <?php get_template_part( 'template-parts/facts-bar' );           // Navigation bar (nav.php) ?>
	</div>

	<?php get_template_part( 'template-parts/video-feature' ); ?>

</section>



<!-- get_sidebar(); -->
<?php
get_footer();
