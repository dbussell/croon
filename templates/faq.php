<?php
/**
  * Template Name: FAQ
 * @package croon
 */
get_header();
?>

<div class="page-header">
	<h1>Info</h1>
</div>

<section id="the-content">

	<div class="content">
    <div class="row">
      <h1>Frequently Asked Questions</h1>
    </div>
	</div>

	<div class="section-facts light">
	  <?php get_template_part( 'template-parts/facts-bar' ); ?>
	</div>

	<?php if( have_rows('questions') ): ?>
		<div class="section-questions">

			<?php while ( have_rows('questions') ) : the_row(); ?>

				<div class="question-wrapper">
					<h3 class="tap">tap!</h3>
					<div class="letter-wrapper question">
						<h2>Q</h2>
					</div>
					<div class="text-wrapper question">
	        	<?php the_sub_field('question'); ?>
					</div>
					<div class="letter-wrapper answer">
						<h2>A</h2>
					</div>
					<div class="text-wrapper answer">
						<?php the_sub_field('answer'); ?>
					</div>
				</div>

		   <?php endwhile; ?>

		</div>
	<?php endif; ?>

	<?php if( have_rows('comparison_chart') ): ?>
    <div class="section-comparison">
      <?php get_template_part( 'template-parts/comparison-chart' ); ?>
    </div>
  <?php endif; ?>


</section>




<?php get_footer();
