<?php
/**
 * croon functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package croon
 */

if ( ! function_exists( 'croon_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function croon_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on croon, use a find and replace
		 * to change 'croon' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'croon', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		add_theme_support( 'custom-logo' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_theme_support( 'post-thumbnails' );
		add_image_size('extra-large', 1500, '', true); // Large Thumbnail
		add_image_size('large', 1200, '', true); // Large Thumbnail
    add_image_size('medium', 900, '', true); // Medium Thumbnail
		add_image_size('medium-small', 600, '', true); // Medium Small Thumbnail
    add_image_size('small', 300, '', true); // Small Thumbnail

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary_menu' => esc_html__( 'Primary', 'croon' ),
			'footer_menu' => esc_html__( 'Footer', 'croon' ),
			'footer_secondary' => esc_html__( 'Footer Secondary', 'croon' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'croon_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'croon_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function croon_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'croon_content_width', 640 );
}
add_action( 'after_setup_theme', 'croon_content_width', 0 );


/**
 * Enqueue scripts and styles.
 */
function croon_scripts() {
	$manifest = json_decode(file_get_contents('dist/assets.json', true));
	$main = $manifest->main;

	wp_enqueue_style( 'croon-style', get_template_directory_uri() . $main->css, false, null );

	wp_enqueue_script('croon-js', get_template_directory_uri() . $main->js, ['jquery'], null, true);

	wp_enqueue_script( 'croon-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'croon-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'croon-cookies', 'https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min.js', array(), '20151215', true );

	wp_enqueue_script( 'croon-interactive-bg', get_template_directory_uri() . '/js/interactive_bg.min.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}


}
add_action( 'wp_enqueue_scripts', 'croon_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function slugify($text)
{
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  $text = preg_replace('~[^-\w]+~', '', $text);
  $text = trim($text, '-');
  $text = preg_replace('~-+~', '-', $text);
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

if( function_exists('acf_add_options_page') ) {

	$option_page = acf_add_options_page(array(
		'page_title' 	=> 'Theme Options',
		'menu_title' 	=> 'Theme Options',
		'menu_slug' 	=> 'footer-content',
		'capability' 	=> 'edit_posts',
		'icon_url' => 'dashicons-editor-insertmore',
		'redirect' 	=> false

	));
}

function add_file_types_to_uploads($file_types){
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types = array_merge($file_types, $new_filetypes );
	return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

function prefix_conditional_body_class( $classes ) {
    if( is_page_template('auditions.php') )
        $classes[] = 'purple';

    return $classes;
}
add_filter( 'mobile-menu', 'prefix_conditional_body_class' );

function addHomeMenuLink($menuItems, $args)
{
    if('mobile-menu' == $args->container_id)
    {
        if ( is_front_page() )
            $class = 'class="current-menu-item"';
        else
            $class = '';
        $homeMenuItem = '<li ' . $class . '>' .
                        $args->before .
                        '<a href="' . home_url( '/' ) . '" title="Home">' .
                            $args->link_before .
                            'Home' .
                            $args->link_after .
                        '</a>' .
                        $args->after .
                        '</li>';
        $menuItems = $homeMenuItem . $menuItems;
    }
    return $menuItems;
}
add_filter( 'wp_nav_menu_items', 'addHomeMenuLink', 10, 2 );

// Receive the Request post that came from AJAX
add_action( 'wp_ajax_demo-pagination-load-posts', 'cvf_demo_pagination_load_posts' );
// We allow non-logged in users to access our pagination
add_action( 'wp_ajax_nopriv_demo-pagination-load-posts', 'cvf_demo_pagination_load_posts' );
function cvf_demo_pagination_load_posts() {

    global $wpdb;
    // Set default variables
    $msg = '';

    if(isset($_POST['page'])){
        // Sanitize the received page
        $page = sanitize_text_field($_POST['page']);
        $cur_page = $page;
        $page -= 1;
        // Set the number of results to display
        $per_page = 4;
        $previous_btn = true;
        $next_btn = true;
        $first_btn = true;
        $last_btn = true;
        $start = $page * $per_page;

        // Set the table where we will be querying data
        $table_name = $wpdb->prefix . "posts";

        // Query the necessary posts
        $all_blog_posts = $wpdb->get_results($wpdb->prepare("
            SELECT * FROM " . $table_name . " WHERE post_type = 'post' AND post_status = 'publish' ORDER BY post_date DESC LIMIT %d, %d", $start, $per_page ) );

        // At the same time, count the number of queried posts
        $count = $wpdb->get_var($wpdb->prepare("
            SELECT COUNT(ID) FROM " . $table_name . " WHERE post_type = 'post' AND post_status = 'publish'", array() ) );

        // Loop into all the posts
        foreach($all_blog_posts as $key => $post):

					$thumb_id = get_post_thumbnail_id($post);
					$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'medium', true);
					$thumb_url = $thumb_url_array[0];
					$postdate = get_the_time(get_option('date_format'), $post->ID);


            // Set the desired output into a variable
            $msg .= '
						<a href="'. get_permalink($post->ID) .'" class="blog-post-wrapper row">
						<div class="image-wrapper col-3">
							<div class="image" style="background-image: url(' . $thumb_url . ')"></div>
						</div>
						<div class="post-meta col-3">
						  <span class="date">' . $postdate . '</span>
						  <h4>' . $post->post_title . '</h4>
						</div>
					  </a>';

        endforeach;

        // Optional, wrap the output into a container
        $msg = "<div class='cvf-universal-content'>" . $msg . "</div><br class = 'clear' />";

        // This is where the magic happens
        $no_of_paginations = ceil($count / $per_page);

        if ($cur_page >= 7) {
            $start_loop = $cur_page - 3;
            if ($no_of_paginations > $cur_page + 3)
                $end_loop = $cur_page + 3;
            else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                $start_loop = $no_of_paginations - 6;
                $end_loop = $no_of_paginations;
            } else {
                $end_loop = $no_of_paginations;
            }
        } else {
            $start_loop = 1;
            if ($no_of_paginations > 7)
                $end_loop = 7;
            else
                $end_loop = $no_of_paginations;
        }

        // Pagination Buttons logic
        $pag_container .= "
        <div class='cvf-universal-pagination'>
            <ul>";

        if ($first_btn && $cur_page > 1) {
            $pag_container .= "<li p='1' class='active'>First</li>";
        } else if ($first_btn) {
            $pag_container .= "<li p='1' class='inactive'>First</li>";
        }

        if ($previous_btn && $cur_page > 1) {
            $pre = $cur_page - 1;
            $pag_container .= "<li p='$pre' class='active'>Previous</li>";
        } else if ($previous_btn) {
            $pag_container .= "<li class='inactive'>Previous</li>";
        }
        for ($i = $start_loop; $i <= $end_loop; $i++) {

            if ($cur_page == $i)
                $pag_container .= "<li p='$i' class = 'selected' >{$i}</li>";
            else
                $pag_container .= "<li p='$i' class='active'>{$i}</li>";
        }

        if ($next_btn && $cur_page < $no_of_paginations) {
            $nex = $cur_page + 1;
            $pag_container .= "<li p='$nex' class='active'>Next</li>";
        } else if ($next_btn) {
            $pag_container .= "<li class='inactive'>Next</li>";
        }

        if ($last_btn && $cur_page < $no_of_paginations) {
            $pag_container .= "<li p='$no_of_paginations' class='active'>Last</li>";
        } else if ($last_btn) {
            $pag_container .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
        }

        $pag_container = $pag_container . "
            </ul>
        </div>";

        // We echo the final output
        echo
        '<div class = "cvf-pagination-content">' . $msg . '</div>' .
        '<div class = "cvf-pagination-nav">' . $pag_container . '</div>';

    }
    // Always exit to avoid further execution
    exit();
}

if (!is_admin()) {
function wpb_search_filter($query) {
if ($query->is_search) {
$query->set('post_type', 'post');
}
return $query;
}
add_filter('pre_get_posts','wpb_search_filter');
}

function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );


// Remove decimals from whole numbers

add_filter( 'woocommerce_price_trim_zeros', '__return_true' );



// Edit account menu

function my_account_menu_order() {
 $menuOrder = array(
	 'dashboard'          => __( 'Profile', 'woocommerce' ),
	 'edit-address'       => __( 'Addresses', 'woocommerce' ),
	 'orders'             => __( 'Purchases', 'woocommerce' ),
	 'customer-logout'    => __( 'Logout', 'woocommerce' ),
 );
 return $menuOrder;
}
add_filter ( 'woocommerce_account_menu_items', 'my_account_menu_order' );

// Add subtitle to columns on products interface

function add_acf_columns ( $columns ) {
	return array_merge ( $columns, array (
		'subtitle' => __ ( 'Type' ),
	) );
}
add_filter ( 'manage_product_posts_columns', 'add_acf_columns' );

function product_custom_column ( $column, $post_id ) {
   switch ( $column ) {
     case 'Type':
       echo get_post_meta ( $post_id, 'subtitle', true );
     break;
   }
 }
 add_action ( 'manage_product_posts_custom_column', 'product_custom_column', 10, 2 );


 // Redirect to Home on Logout

 add_action('wp_logout','auto_redirect_after_logout');

function auto_redirect_after_logout(){

  wp_redirect( home_url() );
  exit();

}

// Redirect to Home on Login (unless Checkout)

add_filter( 'woocommerce_order_amount_shipping_total', 'shipping_total_convert_zero_to_int' );
function shipping_total_convert_zero_to_int( $value ) {
    if($value === '0.00') {
        $value = 0;
    }

    return $value;
}

add_filter( 'woocommerce_cart_shipping_method_full_label', 'remove_local_pickup_free_label', 10, 2 );
function remove_local_pickup_free_label($full_label, $method){
    return $method->label;
}


/*
Plugin Name: Woocommerce Custom Profile Picture
Plugin URI: https://webfor99.com/woocommerce-custom-profile-picture/
Description: Allows any user to upload their own profile picture to the WooCommerce store
Version: 1.0
Author: webfor99.com
Author URI: https://webfor99.com/
*/
// =========================================================================
/**
 * Function wc_cus_cpp_form
 *
 */






// =========================================================================
/**
 * Function wc_cus_save_profile_pic
 *
 */
function wc_cus_save_profile_pic($picture_id, $user_id){
  update_user_meta( $user_id, 'profile_pic', $picture_id );
}
// =========================================================================
/**
 * Function wc_cus_upload_picture
 *
 */
function wc_cus_upload_picture( $foto ) {
  $wordpress_upload_dir = wp_upload_dir();
  // $wordpress_upload_dir['path'] is the full server path to wp-content/uploads/2017/05, for multisite works good as well
  // $wordpress_upload_dir['url'] the absolute URL to the same folder, actually we do not need it, just to show the link to file
  $i = 1; // number of tries when the file with the same name is already exists
  $profilepicture = $foto;
  $new_file_path = $wordpress_upload_dir['path'] . '/' . $profilepicture['name'];

  /* we fixed this, mime_content_type() was not working */
  //$new_file_mime = mime_content_type( $profilepicture['tmp_name'] );
  $check = getimagesize($profilepicture['tmp_name']);
  $new_file_mime = $check["mime"];

  $log = new WC_Logger();

  if( empty( $profilepicture ) )
  $log->add('custom_profile_picture','File is not selected.');
  if( $profilepicture['error'] )
  $log->add('custom_profile_picture',$profilepicture['error']);

  if( $profilepicture['size'] > wp_max_upload_size() )
  $log->add('custom_profile_picture','It is too large than expected.');

  if( !in_array( $new_file_mime, get_allowed_mime_types() ))
  $log->add('custom_profile_picture','WordPress doesn\'t allow this type of uploads.' );
  while( file_exists( $new_file_path ) ) {
  $i++;
  $new_file_path = $wordpress_upload_dir['path'] . '/' . $i . '_' . $profilepicture['name'];
  }
  // looks like everything is OK
  if( move_uploaded_file( $profilepicture['tmp_name'], $new_file_path ) ) {

  $upload_id = wp_insert_attachment( array(
    'guid'           => $new_file_path,
    'post_mime_type' => $new_file_mime,
    'post_title'     => preg_replace( '/\.[^.]+$/', '', $profilepicture['name'] ),
    'post_content'   => '',
    'post_status'    => 'inherit'
  ), $new_file_path );
  /* we fixed this, get_admin_url() was not working by itself */
  // wp_generate_attachment_metadata() won't work if you do not include this file
  require_once( str_replace( get_bloginfo( 'url' ) . '/', ABSPATH, get_admin_url() ) . 'includes/image.php' );
  // Generate and save the attachment metas into the database
  wp_update_attachment_metadata( $upload_id, wp_generate_attachment_metadata( $upload_id, $new_file_path ) );
  return $upload_id;
  }
}
// =========================================================================
/**
 * Function wc_cus_change_avatar
 *
 */
add_filter( 'get_avatar' , 'wc_cus_change_avatar' , 1 , 5 );
function wc_cus_change_avatar( $avatar, $id_or_email, $size, $default, $alt ) {
    $user = false;
    if ( is_numeric( $id_or_email ) ) {
        $id = (int) $id_or_email;
        $user = get_user_by( 'id' , $id );
    } elseif ( is_object( $id_or_email ) ) {
        if ( ! empty( $id_or_email->user_id ) ) {
            $id = (int) $id_or_email->user_id;
            $user = get_user_by( 'id' , $id );
        }
    } else {
        $user = get_user_by( 'email', $id_or_email );
    }
    if ( $user && is_object( $user ) ) {
    $picture_id = get_user_meta($user->data->ID,'profile_pic');
    if(! empty($picture_id)){
      $avatar = wp_get_attachment_url( $picture_id[0] );
      $avatar = "<img alt='{$alt}' src='{$avatar}' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
    }
    }
    return $avatar;
}

// Remove Additional Information Tab
add_filter( 'woocommerce_product_tabs', 'bbloomer_remove_product_tabs', 9999 );

function bbloomer_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] );
    return $tabs;
}

// Remove related products

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

// Do something if refunded
function has_refunds( $order ) {
    return sizeof( $order->get_refunds() ) > 0 ? true : false;
}

// Showing variations in cart, account, and checkout
add_filter( 'woocommerce_product_variation_title_include_attributes', '__return_false' );
add_filter( 'woocommerce_is_attribute_in_product_name', '__return_false' );

// Remove link from product image on product single
function e12_remove_product_image_link( $html, $post_id ) {
    return preg_replace( "!<(a|/a).*?>!", '', $html );
}
add_filter( 'woocommerce_single_product_image_thumbnail_html', 'e12_remove_product_image_link', 10, 2 );

// Move edit account to dashboard
add_action( 'woocommerce_account_dashboard', 'woocommerce_account_edit_account' );

// Add columns to product post list

function my_product_columns($columns)
{
	$columns = array(
		'thumb' 	=> 'Thumbnail',
		'title' 	=> 'Product Name',
		'subtitle'	 	=> 'Subtitle',
		'price'	 	=> 'Price',
		'is_in_stock'	 	=> 'Stock'
	);
	return $columns;
}

function my_custom_product_columns($column, $post_id)
{
	global $post;

	if($column == 'subtitle')
	{
		echo get_field('subtitle');
	}

}

add_action("manage_product_posts_custom_column", "my_custom_product_columns", 10, 2);
add_filter("manage_edit-product_columns", "my_product_columns");

// Add class to body if logged in

function wpd_logged_out_body_class( $classes ) {
    if( is_user_logged_in() ){
        $classes[] = 'logged-in';
    }
    return $classes;
}
add_filter( 'body_class', 'wpd_logged_out_body_class' );

// Remove jetpack frontend CSS
add_filter( 'jetpack_sharing_counts', '__return_false', 99 );
add_filter( 'jetpack_implode_frontend_css', '__return_false', 99 );

// Stop Yoast Thumbnails
add_filter( 'wpseo_og_image', '__return_false' );


/**
 * only copy opening php tag if needed
 * Displays shipping estimates for WC shipping rates
 */
function sv_shipping_method_estimate_label( $label, $method ) {

	$label .= '<br /><small>';

	switch ( $method->method_id ) {
		case 'flat_rate':
			$label .= 'Est delivery: 2-4 days';
			break;

		case 'free_shipping':
			$label .= 'Est delivery: 4-7 days';
			break;

		case 'international_delivery':
			$label .= 'Est delivery: 7-10 days';
			break;

		default:
			$label .= 'Est delivery: 3-5 days';
	}

	$label .= '</small>';
	return $label;
}
add_filter( 'woocommerce_cart_shipping_method_full_label', 'sv_shipping_method_estimate_label', 10, 2 );

// Add a body class via the body_class filter in WP
// =============================================================================
add_filter( 'body_class', 'mb_body_class_for_cart_items' );
function mb_body_class_for_cart_items( $classes ) {
    if( ! WC()->cart->is_empty() ){
        $classes[] = 'cart-has-items';
    }
    return $classes;
}
