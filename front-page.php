<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package croon
 */

get_header();

$image = get_field('hero_image');
$size = 'large';
if( $image ) { $url = wp_get_attachment_url( $image, $size ); }

$params = array('posts_per_page' => -1, 'post_type' => 'product', 'orderby' => 'menu_order', 'order' => 'ASC');
$products_query = new WP_Query($params);

?>

<?php if ($products_query->have_posts()) : ?>
  <div class="products-bar content outer-wrapper-3D bounceInUp animated">
    <div class="inner-wrapper-3D">
      <div class="products-bar-inner-wrapper will-slick">
        <?php while ($products_query->have_posts()) :
          $products_query->the_post();
          $product = get_product(get_the_ID());
        ?>
        <a href="<?php the_permalink(); ?>" class="product">
          <div class="image-wrapper">
            <?php the_post_thumbnail(); ?>
          </div>
          <span class="product-meta">
            <h5><?php the_title(); ?></h5>
            <div class="under-title">
              <?php if( get_field('subtitle') ): ?>
                <h6 class="subtitle"><?php the_field('subtitle'); ?></h6>
                <span class="bullet">&bull;</span>
              <?php endif; ?>
              <h6 class="price">$<?php echo $product->get_price(); ?></h6>
            </div>
          </span>
        </a>
        <?php endwhile; ?>
      </div>
    </div>
  </div>
<?php endif; wp_reset_postdata(); ?>




<div class="section-hero text-centered parallax-background" style="background-image: url(<?php echo $url ?>)" data-ibg-bg="<?php echo $url ?>">


  <div class="gradient"></div>

  <?php if( have_rows('comparison') ): ?>
    <?php while ( have_rows('comparison') ) : the_row(); ?>

  <div class="circles-wrapper will-slick">

    <?php if( have_rows('traditional_makeup_wipes') ): ?>
      <?php while ( have_rows('traditional_makeup_wipes') ) : the_row(); ?>
        <div class="area">
          <div class="circle left">
            <span class="number">
              <div class="label"><?php the_sub_field("label"); ?></div>
              <div class="circle-inner-wrapper">
                  <?php $more_than = get_sub_field('more_than'); if ($more_than && in_array('yes', $more_than)) { ?>
                    <span class="plus">+</span>
                  <?php } ?>
                  <?php the_sub_field("number"); ?>
                </div>
              <div class="units"><?php the_sub_field("units"); ?></div>
            </span>
          </div>
        </div>
      <?php endwhile; ?>
    <?php endif; ?>

    <!--<div class="area center"></div>-->

    <?php if( have_rows('croon') ): ?>
      <?php while ( have_rows('croon') ) : the_row(); ?>
        <div class="area">
          <?php if( get_sub_field("link") ): ?>
            <a href="<?php the_sub_field("link"); ?>">
          <?php endif; ?>
          <div class="circle right" data-micron="tada">
            <span class="number">
              <div class="label"><?php the_sub_field("label"); ?></div>
              <div class="circle-inner-wrapper">
                <?php $more_than = get_sub_field('more_than'); if ($more_than && in_array('yes', $more_than)) { ?>
                  <span class="plus">+</span>
                <?php } ?>

                <?php the_sub_field("number"); ?>
                </div>
              <div class="units"><?php the_sub_field("units"); ?></div>
            </span>
          </div>
          <?php if( get_sub_field("link") ): ?>
            </a>
          <?php endif; ?>
        </div>
      <?php endwhile; ?>
    <?php endif; ?>

  </div>

  <?php endwhile; ?>
<?php endif; ?>


<div class="content animated hero-text">
  <div class="row">
    <div class="col-6">
      <h1 class="text-white font-ag ag7 relative"><?php the_field('heading'); ?></h1>
    </div>
    <div class="col-4">
      <h3 class="text-blue-dark font-starling starling5 relative"><?php the_field('subheading'); ?></h3>
    </div>
  </div>
</div>

</div>

<div class="section-how" id="section-how">

  <div class="circle-button-wrapper animated" id="how-to">
    <a href="#how-to">
      <div class="circle-button down"></div>
    </a>
  </div>

  <?php $image = get_field('product_image');
  $size = 'large'; // (thumbnail, medium, large, full or custom size)
  if( $image ) { $url = wp_get_attachment_url( $image, $size );
  $offset = get_field('offset', $image);
  }
  ?>

  <?php if( have_rows('how_to') ): ?>
    <h2 class="text-white font-ag ag6">How does it work?</h2>
    <h3>tap to find out</h3>

      <div class="quadrants">
        <?php while ( have_rows('how_to') ) : the_row(); ?>
          <div class="row top">
            <div class="quadrant">
              <a href="<?php the_field('link'); ?>" class="desktop">
              <div class="quadrant-content">
                <div class="circle">1</div>
                <div class="quadrant-text">
                  <?php the_sub_field('1'); ?>
                </div>
              </div>
              </a>
              <div class="quadrant-content mobile">
                <div class="circle">1</div>
                <div class="quadrant-text">
                  <?php the_sub_field('1'); ?>
                </div>
              </div>
            </div>
            <div class="quadrant">
              <a href="<?php the_field('link'); ?>" class="desktop">
              <div class="quadrant-content">
                <div class="circle">2</div>
                <div class="quadrant-text">
                  <?php the_sub_field('2'); ?>
                </div>
              </div>
            </a>
            <div class="quadrant-content mobile">
              <div class="circle">2</div>
              <div class="quadrant-text">
                <?php the_sub_field('2'); ?>
              </div>
            </div>
            </div>
          </div>
          <div class="row bottom">
            <div class="quadrant">
              <div class="product-image">
                <div class="gradient-line"></div>
                <img src="<?php echo $url; ?>" class="product-image" style="transform: translateX(<?php echo $offset; ?>%)"/>
              </div>
              <a href="<?php the_field('link'); ?>" class="desktop">
              <div class="quadrant-content">
                <div class="circle">3</div>
                <div class="quadrant-text">
                  <?php the_sub_field('3'); ?>
                </div>
              </div>
            </a>
            <div class="quadrant-content mobile">
              <div class="circle">3</div>
              <div class="quadrant-text">
                <?php the_sub_field('3'); ?>
              </div>
            </div>
            </div>
          </div>
        <?php endwhile; ?>
      </div>

  <?php endif; ?>
</div>

<?php if ($products_query->have_posts()) : ?>
  <div class="section-products">
    <div class="products-carousel will-slick">
      <?php while ($products_query->have_posts()) :
        $products_query->the_post();
        $product = get_product(get_the_ID());
      ?>
      <div class="product">
        <a href="<?php the_permalink(); ?>">
          <div class="circle">
            <div class="zoom-tilt">
              <?php the_post_thumbnail(); ?>
            </div>
          </div>
        </a>
        <div class="product-info">
          <h3>
              <a href="<?php the_permalink(); ?>">
                <?php the_title(); ?>
                <?php if( get_field('subtitle') ): ?>
                  <?php the_field('subtitle'); ?>
                <?php endif; ?>
               </a>
          </h3>
          <h5>$<?php echo $product->get_price(); ?></h5>
        </div>
      </div>
      <?php endwhile; ?>
    </div>
  </div>
<?php endif; wp_reset_postdata(); ?>


<div class="marquee text-white font-ag ag9 ">
  <div class="marquee-inner" aria-hidden="true">
    <div class="text-wrapper"><?php the_field("marquee_text"); ?></div>
    <div class="text-wrapper"><?php the_field("marquee_text"); ?></div>
    <div class="text-wrapper"><?php the_field("marquee_text"); ?></div>
    <div class="text-wrapper"><?php the_field("marquee_text"); ?></div>
  </div>
</div>

<div class="section-facts light">
  <?php get_template_part( 'template-parts/facts-bar' ); ?>
</div>


<?php
$image = get_field('video_background');
$size = 'large'; // (thumbnail, medium, large, full or custom size)
if( $image ) { $url = wp_get_attachment_url( $image, $size ); }
?>

<div class="video-section">
  <div class="video-section-bg slow-zoom" style="background-image: url(<?php echo $url ?>)"></div>
  <div class="video-content">
    <h4 class="font-ag ag8 text-centered text-white">Do more with <span class="text-teal">less.</span></h4>
    <div class="circle-button right video-button hvr-pop"></div>
  </div>
</div>

<?php if( get_field('video', 'option') ): ?>
<div class="video-popup">
  <?php get_template_part( 'template-parts/video-modal' ); ?>
</div>
<?php endif; ?>

<div class="section-review">
  <div class="content">
    <div class="row col-4">
      <div class="stars">
        <img src="<?php echo get_template_directory_uri();?>/images/star.svg" alt="star" />
        <img src="<?php echo get_template_directory_uri();?>/images/star.svg" alt="star" />
        <img src="<?php echo get_template_directory_uri();?>/images/star.svg" alt="star" />
        <img src="<?php echo get_template_directory_uri();?>/images/star.svg" alt="star" />
        <img src="<?php echo get_template_directory_uri();?>/images/star.svg" alt="star" />
      </div>
      <div class="review-text"><?php echo get_field('review') ?></div>
      <div class="review-meta">
        <span class="name"><?php echo get_field('reviewer_name') ?></span>
        <?php if (get_field('reviewer_location')) : ?>
          -
          <span class="location"><?php echo get_field('reviewer_location') ?></span>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>

<?php if( have_rows('comparison_chart') ): ?>
  <div class="section-comparison">
    <?php get_template_part( 'template-parts/comparison-chart' ); ?>
  </div>
<?php endif; ?>



<?php
wp_reset_postdata();
get_footer();
