<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package croon
 */
global $woocommerce;

global $current_user;
get_currentuserinfo();


switch (true)  {
	case ( !is_user_logged_in() ):
    $user_link = home_url('/account');
  break;
	case ( is_user_logged_in() ):
    $user_link = home_url('/account');
  break;

}

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129517174-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-129517174-1');
	</script>

	<title><?php echo get_option( 'blogname' ); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/images/social-share.jpg" />
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.typekit.net/cve5qaw.css">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/images/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon/safari-pinned-tab.svg" color="#533a7b">
	<link href="https://unpkg.com/webkul-micron@1.1.6/dist/css/micron.min.css" type="text/css" rel="stylesheet">
	<script src="https://unpkg.com/webkul-micron@1.1.6/dist/script/micron.min.js" type="text/javascript"></script>
	<meta name="msapplication-TileColor" content="#000000">
	<meta name="theme-color" content="#533a7b">
	<script src="<?php echo get_template_directory_uri() . '/js/jquery.in-viewport-class.js'; ?></script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header id="masthead" class="site-header">

						<div class="header-bar">

							<nav id="site-navigation" role="navigation" class="main-navigation main-navigation-mobile">
							  <div class="menu-button">
							    <input type="checkbox" id="menu-input" />
									<div class="hamburger">
										<span class="top"></span>
										<span class="bottom"></span>
									</div>
									<div class="menu-wrapper">
										<div class="circle"></div>
										<?php wp_nav_menu( array( 'theme_location' => 'primary_menu') ); ?>
										<div class="account-link">
										<?php if (is_user_logged_in()) : ?>
										    <a href="<?php echo wp_logout_url(get_permalink()); ?>">Logout</a>
										<?php else : ?>
										    <a href="<?php echo $user_link; ?>">Login</a>
										<?php endif;?>
										</div>
										<div class="gradient"></div>
									</div>
							  </div>
							</nav>

							<div class="logo-outer-wrapper">

								<div class="tag-wrapper">

									<div class="added-to-cart bounceInUpSmall animated">
										<div class="exclamation">!</div>
										<div class="message">Added to cart!</div>
									</div>

									<div class="cart-close-wrapper in-header">
										<div class="cart-close-inner-wrapper">
											<span class="top"></span>
											<span class="bottom"></span>
										</div>
									</div>

									<a href="<?php echo $user_link; ?>" class="user-image" style="background-image: url('<?php echo $avatar_url; ?>')"/>
											<img src="<?php echo get_template_directory_uri();?>/images/user.svg" alt="user" />
											<?php if ( $user && is_object( $user ) ) {
										    $picture_id = get_user_meta($user->data->ID,'profile_pic');
										    if(! empty($picture_id)){
										      $avatar = wp_get_attachment_url( $picture_id[0] );
										      $avatar = "<img alt='{$alt}' src='{$avatar}' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
													return $avatar;
												}
										    }
												?>
									</a>
									<a href="<?php echo home_url(); ?>" alt="home" class="logo site-title">
										<div class="logo-inner-wrapper">
											<img src="<?php echo get_template_directory_uri(); ?>/images/logo-white.svg" alt="<?php echo get_option( 'blogname' ); ?>" />
										</div>
									</a>
									<div class="cart-count">
										<div class="circle">
											<?php echo $woocommerce->cart->cart_contents_count; ?>
										</div>
									</div>



								<input type="checkbox" class="cart-checkbox" id="cart-input" />
								<div class="cart-wrapper">
									<div class="circle"></div>
										<div class="cart">

											<div class="cart-close-wrapper in-cart">
												<div class="cart-close-inner-wrapper">
													<span class="top"></span>
													<span class="bottom"></span>
												</div>
											</div>

											<h2>Your Bag</h2>
											<?php get_template_part( 'woocommerce/cart/mini-cart' ); ?>
									</div>
								</div>



							</div>

						</div>

	</header>

	<div id="content" class="site-content">
