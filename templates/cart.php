<?php
/**
  * Template Name: Cart
 * @package croon
 */
get_header();
?>

<div class="page-header">
	<h1>cart</h1>
</div>

<section class="error-404 not-found">
	<div class="circle big">
		<div class="circle small"></div>
	</div>
	<div class="content message">
		<h2>There's nothing here!</h2>
		<div class='bar-button'>
			<a class="button wc-backward" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
				<?php esc_html_e( 'Return to shop', 'woocommerce' ); ?>
			</a>
		</div>
	</div>
</section>

<?php
get_footer();
