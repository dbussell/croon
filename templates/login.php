<?php
//Template Name:  Login
if ( is_user_logged_in() ) {
  wp_redirect(home_url()/login);
}
$image = get_field('background_image');
$size = 'full';
if( $image ) { $url = wp_get_attachment_url( $image, $size ); }


  $login_args = array(
    'redirect' => home_url(),
    'form_id' => 'croon-login',
    'label_username' => __( 'Username' ),
    'label_password' => __( 'Password' ),
    'label_remember' => __( 'Remember Me' ),
    'label_log_in' => __( 'Log In' ),
    'remember' => true
  );

if (isset($_POST['user_registeration']))
{
    //registration_validation($_POST['username'], $_POST['useremail']);
    global $reg_errors;
    $reg_errors = new WP_Error;
    $username=$_POST['username'];
    $useremail=$_POST['useremail'];
    $password=$_POST['password'];


    if(empty( $useremail ) || empty($password))
    {
        $reg_errors->add('field', 'Required form field is missing');
    }
    if ( !is_email( $useremail ) )
    {
        $reg_errors->add( 'email_invalid', 'Email id is not valid!' );
    }

    if ( email_exists( $useremail ) )
    {
        $reg_errors->add( 'email', 'Email Already exist!' );
    }
    if ( 5 > strlen( $password ) ) {
        $reg_errors->add( 'password', 'Password length must be greater than 5!' );
    }

    if (is_wp_error( $reg_errors ))
    {
        foreach ( $reg_errors->get_error_messages() as $error )
        {
             $signUpError='<p style="color:#FF0000; text-aling:left;"><strong>ERROR</strong>: '.$error . '<br /></p>';
        }
    }


    if ( 1 > count( $reg_errors->get_error_messages() ) )
    {
        // sanitize user form input
        global $useremail;
        $useremail  =   sanitize_email( $_POST['useremail'] );
        $password   =   esc_attr( $_POST['password'] );

        $userdata = array(
            'user_email'    =>   $useremail,
            'user_pass'     =>   $password,
            );
        $user = wp_insert_user( $userdata );
    }

}


get_header(); ?>

<main style="background-image: url(<?php echo $url ?>)">


  <div class="page-header">
  	<h1>Log In</h1>
  </div>

  <section class="section-login">
    <div class="content">
      <div class="row">

        <div class="col-3 login-wrapper">
          <div class="outer-wrapper-3D">
            <div class="inner-wrapper-3D">
              <div class="login-inner-wrapper">
                <h3>Log In</h3>
                <?php
                  wp_login_form( $login_args );
                ?>
                <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="forgot">Lost your password?</a>
              </div>
            </div>
          </div>
        </div>

      	<div class="circle">
        	<div class="content">
        		<h2>New to Croon?</h2>
        		<h3>Create an account</h3>
            <form action="" method="post" name="user_registration">
              <label>Email address <span class="error">*</span></label>
              <input type="text" name="useremail" class="input" placeholder="email" required /> <br />
              <label>Password <span class="error">*</span></label>
              <input type="password" name="password" class="input password" placeholder="password" required /> <br />
              <input type="submit" name="user_registeration" value="Create Account" class="button"/>
            </form>
            <?php if(isset($signUpError)){echo '<div>'.$signUpError.'</div>';}?>
        	</div>
        </div>

      </div>
    </div>
  </section>

</main>




<?php get_footer(); ?>
